resource "google_compute_instance" "ubuntu_instance" {
  name         = "ubuntu-instance"
  machine_type = "n1-standard-1"
  zone         = "us-central1-a"  
  tags         = ["puppet"]

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
    }
  }

  network_interface {
    network = "default"
  }

  metadata_startup_script = file("./puppet/startup_script.sh")
}
