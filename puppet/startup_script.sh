#!/bin/bash

# Install Puppet
apt-get update
apt-get install -y puppet

# Create Puppet Manifests directory
mkdir -p /etc/puppet/manifests

# Download Puppet Manifests
wget -O /etc/puppet/manifests/site.pp https://gitlab.com/i-amit-singh/terraform/-/blob/d0597ac11bd3b428f6547b7c4d344aed76f3a273/puppet/site.pp

# Apply Puppet Manifests
puppet apply /etc/puppet/manifests/site.pp
