
provider "google" {
  credentials = file("inbound-vim-415408-bb2670a904e2.json")
  project     = "inbound-vim-415408"
  region      = "us-central1"
}
module "puppet_instance" {
  source = "./terraform"
}